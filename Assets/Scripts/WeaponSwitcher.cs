﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSwitcher : MonoBehaviour
{
    [SerializeField] int currentWeapon = 0;

    void Start()
    {
        SetWeaponActive();
    }

    void Update()
    {
        int previousWeapon = currentWeapon;

        ProcessKeyInput();
        ProcessScrollWheel();

        if (previousWeapon != currentWeapon)
        {
            SetWeaponActive();
        }
    }

    private void ProcessScrollWheel()
    {
        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            if (currentWeapon >= transform.childCount - 1) // если оружие максимальное - самое последнее, если крутить вверх
            {
                currentWeapon = 0;
            }
            else
            {
                currentWeapon++; // то просто при прокрутке вверх меняй оружие на 1
            }
        }

        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            if (currentWeapon <= 0) // если оружие минимальное - самое первое, если крутить вниз
            {
                currentWeapon = transform.childCount - 1;
            }
            else
            {
                currentWeapon--; // то просто при прокрутке вниз меняй оружие на 1
            }
        }
    }

    private void ProcessKeyInput()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1)) // 1 на клаве
        {
            currentWeapon = 0;
        }

        if (Input.GetKeyDown(KeyCode.Alpha2)) // 1 на клаве
        {
            currentWeapon = 1;
        }

        if (Input.GetKeyDown(KeyCode.Alpha3)) // 1 на клаве
        {
            currentWeapon = 2;
        }
    }

    private void SetWeaponActive()
    {
        int weaponIndex = 0;

        foreach (Transform weapon in transform)
        {
            if (weaponIndex == currentWeapon)
            {
                weapon.gameObject.SetActive(true);
            }
            else
            {
                weapon.gameObject.SetActive(false);
            }
            weaponIndex++;
        }
    }

    
}
